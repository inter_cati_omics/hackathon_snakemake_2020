# Short tutorial  
  
source: https://snakemake.readthedocs.io/en/stable/tutorial/short.html  

## Pré-requis  
  
### Snakemake
  
Le tutoriel court officiel vous recommande l'[installation de snakemake via conda](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html#conda-install).  
Néanmoins sur genotoul, snakemake est disponible sous forme de modules prêt à charger:  
  
```bash
# modules snakemake sur genotoul
module av 2>&1 | grep snakemake
bioinfo/fastx_toolkit-0.0.14           bioinfo/snakemake-4.5.1
bioinfo/FBAT_v204                      bioinfo/snakemake-4.8.0
bioinfo/FEELnc-v.0.1.1                 bioinfo/snakemake-5.8.1

```  
  
On peut donc charger un des modules snakemake, selon la version choisie, sans avoir à installer snakemake, comme ceci:  
  
```bash
# chargement du module snakemake 5.8.1  
module load bioinfo/snakemake-5.8.1

```  
  
### Conda  
   
Si vous souhaitez suivre le tutoriel original il vous faudra aussi conda.
Plusieurs options sont disponibles, vous pouvez alors:
* installer miniconda3 vous même en suivant ces [instructions](https://snakemake.readthedocs.io/en/stable/tutorial/setup.html#step-1-installing-miniconda-3)  
* charger le module miniconda  
  
Dans ce dernier cas, voici les commandes à exécuter:  
  
```bash
# modules miniconda disponibles sur genotoul  
module av 2>&1| grep conda
bioinfo/biopieces-8752df5              bioinfo/pbbioconda
bioinfo/Bismark-0.22.3                 bioinfo/pbbioconda-12082020
bioinfo/Bismark_v0.19.0                bioinfo/pbbioconda-805fb87
bioinfo/mapDamage-2.0.8                system/Anaconda3-5.2.0
bioinfo/megahit_v1.1.3                 system/Miniconda3-4.4.10
bioinfo/megalodon-v1.0.0               system/Miniconda3-4.7.10


# chargement du module miniconda choisi
module load system/Miniconda3-4.7.10 

```

### Singularity   
  
Si vous souhaitez adapter le tuoriel à l'utilisation de singularity, il vous faudra charger le module singularity.  
   
Voici les commandes à exécuter sur genotoul:  
  
```bash
# modules singularity disponibles sur genotoul  
module av 2>&1 | grep singularity

# chargement du module singularity choisi  
module load system/singularity-3.5.3

``` 
  
### Jeu de données test  
  
Il est disponible à cet url: https://github.com/snakemake/snakemake-tutorial-data
  
Et vous pouvez exécuter les commandes suivantes pour les télécharger:  
  
```bash
mkdir snakemake-demo
cd snakemake-demo
wget https://github.com/snakemake/snakemake-tutorial-data/archive/v5.4.5.tar.gz
tar --wildcards -xf v5.4.5.tar.gz --strip 1 "*/data"

```

Les données test sont téléchargés dans un dossier de travail `snakemake-demo` dans le sous-dossier `data/`.  
  
## Short tutorial avec CONDA  
 
Pour travailler le tutoriel, je vous recommande de lancer une session shell interactive sur le cluster:  

```bash
# lancement d'une session interactive avec 2 cpus et 4G de RAM
srun -p workq --mem 4G -c 2 --pty bash

```
  
  
Retrouvez les solutions au tutoriel [ici](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda):  
1. [step1](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda/step1)
2. [step2](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda/step2)
3. [step3](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda/step3)
4. [step4](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda/step4)
5. [step5](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda/step5)
6. [step6](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda/step6)
7. [step7](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda/step7)
8. [step8](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/conda/step8)  
  
## Short tutorial avec Singularity  
 
`WORK IN PROGRESS`   
  
Il s'agit de d'utiliser singularity pour déployer les dépendances logicielles.   
  
Il y a plusieurs possibilités:  
* utiliser singularity + conda  
* utiliser singularity seul  
  
### Singularity + conda  

Dans ce cas de figure, on utilise une image singularity qui contient miniconda pour gérer le déploiement des packages conda.  
  
```bash
# Snakefile
singularity: "docker://continuumio/miniconda3:4.7.10"

```
  
On peut utiliser cette directive globale pour pointer vers cette image dans le Snakefile.  
Puis au moment de lancer snakemake, ustiliser les options: `--use-conda` et `--use-singularity` pour activer son utilisation dans le workflow.  

### Singularity  

Pour ce short tutorial j'ai créé des images singularity (mapping, calling, stats) pour remplacer les environnements conda précédents.    
Vous pouvez en savoir plus sur ses images ici:  
* https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_singularity_mapping  
* https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_singularity_calling  
* https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_singularity_stats  
  
  
#### Exemple d'utilisation avec l'environnement de mapping:  
  
J'ai créé une recette singularity pour créer un environnement avec le stack logiciels utilisé pour le mapping dans le short tutoriel snakemake. Vous pouvez retrouver cette recette [ici](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_singularity_mapping).  
Et récupérer l'image depuis genotoul avec cette commande:  
  
```bash
# plus besoin de s'authentifier avec --docker-login depuis le passage du projet en public
SINGULARITY_DISABLE_CACHE=0 singularity --debug pull snakemake_mapping.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_singularity_mapping/hackathon_snakemake_singularity_mapping:latest

```  

Il faut réactiver l'utilisation du cache, `SINGULARITY_DISABLE_CACHE=0`, si on veut qua la commande `singularity pull` fonctionne sur genotoul.    
  
  
Et encore plus simple, utiliser l'url oras dans la directive singularity pour déployer directement l'image singularity depuis la forge au moment de l'exécution du workflow.  
  
```python
# Snakefile
singularity: "oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_singularity_mapping/hackathon_snakemake_singularity_mapping:latest"

```
  
Et utiliser l'option `--use-singularity` pour activer l'utilisation de singularity par snakemake.  
  
#### [Solutions au short tutorial avec singularity](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity)  

1. [step1](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity/step1)  
2. [step2](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity/step2)  
3. [step3](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity/step3)  
4. [step4](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity/step4)  
5. [step5](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity/step5)  
6. [step6](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity/step6)  
7. [step7](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity/step7)  
8. [step8](https://forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_2020/-/tree/master/short_tutorial/snakemake-demo/singularity/step8)  
