#!/bin/bash

# load required modules
module load system/Miniconda3-4.7.10
module load bioinfo/snakemake-5.8.1

# dry-run
snakemake --use-conda -n plots/quals.svg

# run
snakemake --use-conda --cores 1 plots/quals.svg
