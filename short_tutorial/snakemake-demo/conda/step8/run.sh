#!/bin/bash

# load required modules
module load system/Miniconda3-4.7.10
module load bioinfo/snakemake-5.8.1

# should install these python modules once
# pip install --user Jinja2 # snakemake reporting requires jinja2 python module
# pip install --user networkx pygraphviz # also mandatory to create report
# pip install --user pygments # again another dependency
# fix python path
export PYTHONPATH=/usr/local/bioinfo/src/Snakemake/snakemake-5.8.1_venv/lib64/python3.6/site-packages:$HOME/.local/lib/python3.6/site-packages

# dry-run
snakemake --use-conda -n

# run
snakemake --use-conda --cores 1
