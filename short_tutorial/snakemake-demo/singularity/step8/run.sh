#!/bin/bash

# load required modules
module load system/singularity-3.5.3
module load bioinfo/snakemake-5.8.1

# pip install --user Jinja2 # snakemake reporting requires jinja2 python module
# pip install --user networkx pygraphviz # also mandatory to create report
# pip install --user pygments # again another dependency
export PYTHONPATH=/usr/local/bioinfo/src/Snakemake/snakemake-5.8.1_venv/lib64/python3.6/site-packages:$HOME/.local/lib/python3.6/site-pa
ckages

# singularity pull error workaround
SINGULARITY_DISABLE_CACHE=0

# dry-run
snakemake --use-singularity -n

# run
snakemake --use-singularity --cores 1
