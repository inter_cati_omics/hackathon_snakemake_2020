#!/bin/bash

# load required modules
module load system/singularity-3.5.3
module load bioinfo/snakemake-5.8.1

# singularity pull error workaround
SINGULARITY_DISABLE_CACHE=0

# dry-run
snakemake --use-singulariy -n plots/quals.svg

# run
snakemake --use-singularity --cores 1 plots/quals.svg
