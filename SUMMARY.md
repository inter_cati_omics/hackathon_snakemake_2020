# Summary

* [Introduction](README.md)  
* [Short tutorial](short_tutorial/README.md)  
    * [Pré-requis](short_tutorial/README.md#pre-requis)
    * [Avec CONDA](short_tutorial/README.md#short-tutorial-avec-conda)
    * [Avec Singularity](short_tutorial/README.md#short-tutorial-avec-singularity)
* [Snakemake tutorial](snakemake_tutorial/README.md)  
  

