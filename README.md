# Hackathon Snakemake inter-CATI 2020  
![snakemake](img/snakemake_logo.png)

---

Sur ce site vous trouverez les supports aux tutoriels snakemake.  
Ces supports sont adaptés des tutoriels officiels snakemake ([short_tutorial](https://snakemake.readthedocs.io/en/stable/tutorial/short.html) et [Snakemake tutorial](https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html)) pour être exécutés sur la [plateforme bioinformatique genotoul](http://bioinfo.genotoul.fr/) en utilisant [conda](https://docs.conda.io/en/latest/) et la technologie de conteneurs [singularity](https://sylabs.io/).  

## Gitlab pages  
  
* [gitbook snakemake](https://inter_cati_omics.pages.mia.inra.fr/hackathon_snakemake_2020/)    
  
## Salon de visioconférence  
  
* http://genobbb.inrae.fr/b/bio-9ec-k2a-ddw    
  
## Pads pour la prise de notes  
  
* https://mensuel.framapad.org/p/intercati_visio_snakemake-9jvq   
  
## Pré-requis  
  
* Avoir un compte sur genotoul  
* Avoir un terminal pour la connexion ssh  

## Introduction à Snakemake  
  
* [slides](https://slides.com/johanneskoester/snakemake-tutorial)   
  
## [Short Tutorial](short_tutorial/README.md)  
  
## [Snakemake tutorial](snakemake_tutorial/README.md)  
  
## Documentation  
  
* [Site officiel](https://snakemake.github.io/) 
* [Voir l'intro](https://www.youtube.com/watch?v=UOKxta3061g)  
* [Tutoriel vidéo](https://www.youtube.com/watch?v=hPrXcUUp70Y)  
* [Read the docs](https://snakemake.readthedocs.io/)  
* [Read the manuscript](https://doi.org/10.5281/zenodo.4063462)  
* [Snakemake on github](https://github.com/snakemake/snakemake)  
  
  
  

